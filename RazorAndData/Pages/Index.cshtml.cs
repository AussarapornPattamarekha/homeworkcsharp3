﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RazorAndData.Models;
using RazorAndData.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorAndData.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        public JsonFileProductService ProductService;
        public IEnumerable<Product> Products;

        public IndexModel(ILogger<IndexModel> logger, JsonFileProductService _productService)
        {
            _logger = logger;
            ProductService = _productService;
        }

        public void OnGet()
        {
            Products = ProductService.GetProducts();
        }
    }
}
