//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.RazorPages;
//using Microsoft.Extensions.Logging;
//using RazorAndData.Models;
//using RazorAndData.Services;using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RazorAndData.Models;
using RazorAndData.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorAndData.Pages
{
    public class PhotoPageModel : PageModel
    {
        private readonly ILogger<PhotoPageModel> _logger;
        public JsonFilePhotoService PhotoService;
        public IEnumerable<Photo> Photos;

        public PhotoPageModel(ILogger<PhotoPageModel> logger, JsonFilePhotoService _photoService)
        {
            _logger = logger;
            PhotoService = _photoService;
        }
        public void OnGet()
        {
            Photos = PhotoService.GetPhotos();
        }
    }
}
